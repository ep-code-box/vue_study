import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import DataBinding from '../views/DataBinding.vue'
import DataBindingHtml from '../views/DataBindingHtml.vue'
import DataBindingInputText from '../views/DataBindingInputText.vue'
import DataBindingInputNumber from '../views/DataBindingInputNumber.vue'
import TextArea from '../views/TextArea.vue'
import Select from '../views/Select.vue'
import CheckBox from '../views/CheckBox.vue'
import Radio from '../views/Radio.vue'
import imgBinding from '../views/Attribute/imgBinding'
import disableButton from '../views/Attribute/disableButton'
import ClassBinding from '../views/Attribute/ClassBinding'
import VFor from '../views/Attribute/v-for'
import SeriesV from '../views/Attribute/SeriesV'
import Event from '../views/event/event'
import ComputedAndWatch from '../views/event/ComputedAndWatch'
import CommuList from '../views/community/CommuList'
import nc from '../views/parentChild/NestedComponent'
import EventParent from '../views/parentChild/EventParent'
import ProvideInject from '../views/provideInject/parent'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/databinding',
    name: 'DataBinding',
    component: DataBinding
  },
  {
    path: '/databindinghtml',
    name: 'DataBindingHtml',
    component: DataBindingHtml
  },
  {
    path: '/databindinginputtext',
    name: 'DataBindingInputText',
    component: DataBindingInputText
  },
  {
    path: '/databindinginputnumber',
    name: 'DataBindingInputNumber',
    component: DataBindingInputNumber
  },
  {
    path: '/textarea',
    name: 'TextArea',
    component: TextArea
  },
  {
    path: '/select',
    name: 'Select',
    component: Select
  },
  {
    path: '/checkbox',
    name: 'CheckBox',
    component: CheckBox
  },
  {
    path: '/radio',
    name: 'Radio',
    component: Radio
  },
  {
    path: '/imgbinding',
    name: 'imgBinding',
    component: imgBinding
  },
  {
    path: '/button',
    name: 'attributeButton',
    component: disableButton
  },
  {
    path: '/classbinding',
    name: 'ClassBinding',
    component: ClassBinding
  },
  {
    path: '/tableV-for',
    name: 'v-for',
    component: VFor
  },
  {
    path: '/seriesv',
    name: 'SeriesV',
    component: SeriesV
  },
  {
    path: '/event',
    name: 'Event',
    component: Event
  },
  {
    path: '/caw',
    name: 'ComputedAndWatch',
    component: ComputedAndWatch
  },
  {
    path: '/commulist',
    name: 'CommuList',
    component: CommuList
  },
  {
    path: '/nc',
    name: 'nc',
    component: nc
  },
  {
    path: '/ptoc',
    name: 'EventParent',
    component: EventParent
  },
  {
    path: '/provideinject',
    name: 'ProvideInject',
    component: ProvideInject
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
